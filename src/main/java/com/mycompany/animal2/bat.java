/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal2;

/**
 *
 * @author admin
 */
public class bat extends Poultry{
    private String name;

    public bat (String name) { 
        super("bat",2);
        this.name = name;
    }

    @Override
    public void fly() {
        System.out.println("bat : " + name + " can fly");
    }

    @Override
    public void eat() {
       System.out.println("bat : " + name + " can eat");
    }
    
    @Override
    public void walk() {
       System.out.println("bat : " + name + " walk with " + numberOfLeg + " legs.");
    }

    @Override
    public void speak() {
        System.out.println("bat : " + name + " can speak");
    }

    @Override
    public void sleep() {
        System.out.println("bat : " + name + " can sleep");
    }

}
