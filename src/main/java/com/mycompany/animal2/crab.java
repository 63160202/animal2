/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal2;

/**
 *
 * @author admin
 */
public class crab extends AquaticAnimal {
    private String name;

    public crab (String name) { 
        super("crab", 8);
        this.name = name;
    }

    @Override
    public void swim() {
        System.out.println("crab : " + name + " can swim");
    }

    @Override
    public void eat() {
       System.out.println("crab : " + name + " can eat");
    }
    
    @Override
    public void walk() {
       System.out.println("crab : " + name +  " walk with " + numberOfLeg  + " legs." );
    }

    @Override
    public void speak() {
        System.out.println("crab : " + name + " can speak");
    }

    @Override
    public void sleep() {
        System.out.println("crab : " + name + " can sleep");
    }
    
}
