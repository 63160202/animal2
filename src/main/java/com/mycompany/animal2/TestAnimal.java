/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal2;

/**
 *
 * @author admin
 */
public class TestAnimal {
    public static void main(String[] args) {
        Human h1 = new Human("lucky");
        h1.run();
        h1.eat();
        h1.walk();
        h1.speak();
        h1.sleep();
        System.out.println(h1.toString());
        
        System.out.println("================================================");
        
        cat c1 = new cat("jep");
        c1.run();
        c1.eat();
        c1.walk();
        c1.speak();
        c1.sleep();
        System.out.println(c1.toString());
        
        System.out.println("================================================");
        
        dog d1 = new dog("jeno");
        d1.run();
        d1.eat();
        d1.walk();
        d1.speak();
        d1.sleep();
        System.out.println(d1.toString());

        System.out.println("================================================");
        
        crocodile cd1 = new crocodile("mark");
        cd1.crawl();
        cd1.eat();
        cd1.walk();
        cd1.speak();
        cd1.sleep();
        System.out.println(cd1.toString());

        System.out.println("================================================");
        
        snake  s1 = new snake("bam");
        s1.crawl();
        s1.eat();
        s1.walk();
        s1.speak();
        s1.sleep();
        System.out.println(s1.toString());

        System.out.println("================================================");
        
        fish  f1 = new fish("liuyu");
        f1.swim();
        f1.eat();
        f1.walk();
        f1.speak();
        f1.sleep();
        System.out.println(f1.toString());
        
        System.out.println("================================================");
        
        crab  cb1 = new crab("yifan");
        cb1.swim();
        cb1.eat();
        cb1.walk();
        cb1.speak();
        cb1.sleep();
        System.out.println(cb1.toString());

        System.out.println("================================================");
        
        bird  bd1 = new bird("ahgase");
        bd1.fly();
        bd1.eat();
        bd1.walk();
        bd1.speak();
        bd1.sleep();
        System.out.println(bd1.toString());

        System.out.println("================================================");
        
        bat  bt1 = new bat("linmo");
        bt1.fly();
        bt1.eat();
        bt1.walk();
        bt1.speak();
        bt1.sleep();
        System.out.println(bt1.toString());

        System.out.println("================================================");
        
        Animal animals [] = {h1,c1,d1,cd1,s1,f1,cb1,bd1,bt1};
        for (int i = 0; i < animals.length; i++) {
            System.out.println(animals[i].getName()+" instanceof Animal : "+(animals[i] instanceof  Animal));
            System.out.println(animals[i].getName()+" instanceof LandAnimal : "+(animals[i] instanceof LandAnimal));
            System.out.println(animals[i].getName()+" instanceof Reptile : "+(animals[i] instanceof Reptile));
            System.out.println(animals[i].getName()+" instanceof AquaticAnimal : "+(animals[i] instanceof AquaticAnimal));
            System.out.println(animals[i].getName()+" instanceof Poultry : "+(animals[i] instanceof Poultry));
        }
        
    }
}
