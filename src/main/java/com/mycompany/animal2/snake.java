/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal2;

/**
 *
 * @author admin
 */
public class snake extends Reptile{
    private String name;

    public snake(String name) { 
        super("snake",0);
        this.name = name;
    }

    @Override
    public void crawl() {
        System.out.println("snake : " + name + " can crawl");
    }

    @Override
    public void eat() {
       System.out.println("snake : " + name + " can eat");
    }
    
    @Override
    public void walk() {
       System.out.println("snake : " + name + " can't walk!!!" );
    }

    @Override
    public void speak() {
        System.out.println("snake : " + name + " speak : chu chu!!!");
    }

    @Override
    public void sleep() {
        System.out.println("snake : " + name + " can sleep");
    }


}

