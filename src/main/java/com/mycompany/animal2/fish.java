/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal2;

/**
 *
 * @author admin
 */
public class fish extends AquaticAnimal {
    private String name;

    public fish (String name) { 
        super("fish",0);
        this.name = name;
    }

    @Override
    public void swim() {
        System.out.println("fish : " + name + " can swim");
    }

    @Override
    public void eat() {
       System.out.println("fish : " + name + " can eat");
    }
    
    @Override
    public void walk() {
       System.out.println("fish : " + name + " can't walk!!!" );
    }

    @Override
    public void speak() {
        System.out.println("fish : " + name + " can speak");
    }

    @Override
    public void sleep() {
        System.out.println("fish : " + name + " can sleep");
    }
}
