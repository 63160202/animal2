/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal2;

/**
 *
 * @author admin
 */
public class dog extends LandAnimal {
    private String name;

    public dog(String name) { 
        super("dog", 4);
        this.name = name;
    }

    @Override
    public void run() {
        System.out.println("dog : " + name + " can run");
    }

    @Override
    public void eat() {
       System.out.println("dog : " + name + " can eat");
    }

    @Override
    public void walk() {
       System.out.println("dog : " + name + " walk with " + numberOfLeg + " legs.");
    }

    @Override
    public void speak() {
        System.out.println("dog : " + name + " speak : box box -.-");
    }

    @Override
    public void sleep() {
        System.out.println("dog : " + name + " can sleep");
    }
}
