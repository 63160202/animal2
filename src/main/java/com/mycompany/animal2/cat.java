/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal2;

/**
 *
 * @author admin
 */
public class cat extends LandAnimal{
    private String name;

    public cat(String name) { 
        super("cat", 4);
        this.name = name;
    }

    @Override
    public void run() {
        System.out.println("cat : " + name + " can run");
    }

    @Override
    public void eat() {
       System.out.println("cat : " + name + " can eat");
    }

    @Override
    public void walk() {
       System.out.println("cat : " + name + " walk with " + numberOfLeg + " legs.");
    }

    @Override
    public void speak() {
        System.out.println("cat : " + name + " speak : meow meow ^^");
    }

    @Override
    public void sleep() {
        System.out.println("cat : " + name + " can sleep");
    }
    

}
