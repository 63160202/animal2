/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal2;

/**
 *
 * @author admin
 */
public class bird extends Poultry{
    private String name;

    public bird (String name) { 
        super("bird",2);
        this.name = name;
    }

    @Override
    public void fly() {
        System.out.println("bird : " + name + " can fly");
    }

    @Override
    public void eat() {
       System.out.println("bird : " + name + " can eat");
    }
    
    @Override
    public void walk() {
       System.out.println("bird : " + name +  " walk with " + numberOfLeg + " legs." );
    }

    @Override
    public void speak() {
        System.out.println("bird : " + name + " speak : jeep jeep ");
    }

    @Override
    public void sleep() {
        System.out.println("bird : " + name + " can sleep");
    }
}
